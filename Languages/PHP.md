PHP Language
==============

Coding guideline

## 1. File Format

### 1.1. Unix file format

All files must be in unix file format. This means \n as a newline, not \r\n.

### 1.2. Maximum Line Length

132 characters, all lines must be broken on/before 132 characters. This includes taking indentation into account.

For instance 3 indents is 12 characters.


## 2. Whitespaces

### 2.1. No whitespaces at the end of the lines

No whitespaces are permitted at the end of lines.

### 2.2. No mixed space+tab for indentation.

No mixed space+tab combinations are allowed for indentation. All indentation must use tabs.

### 2.3. Empty lines must be empty, no whitespaces.

No whitespaces are permitted on empty lines.


## 3. Identation

### 3.1. Tab indentation is used, with the tab width being 4 spaces

Set your editor to use 4 space widths for tabs.

### 3.2.1. Continuation lines are indented twice

For example...

```php
if (joesoap &&
		test) {
}

echo "hi there ".
		"joe soap\n";
```

### 3.2.2. Continuation lines in HTML are indented twice

Example...

```php
<input type='button' value='<?php echo __("Edit"); ?>' name='edit_attribute'
		onclick='editAttributeRow(<?php echo $i; ?>);' class='btn btn-primary'/>
```

### 3.3. Split up lines are indented once

For example...

```php
if (
	joesoap && test
) {
}

sprintf("%s",
	$test
);

$test = '
	test
';
```


## 4. Functions

### 4.1. Function definitions occur on one line with the opening and closing brace on their own lines...

For example...

```php
function testme($hi)
{
	return "hi";
}
```

### 4.2. Three empty lines must be present between functions

For example...

```php
function a()
{
}



function b()
{
}
```

### 4.3. Two empty lines between function parameters and code

This is rarely used in PHP.

For example...

```php
function a($b)
{
	$myvar = NULL;


	if (!empty($b)) {
		$myvar = 1;
	}

	return $myvar;
}
```


## 5. Documenting code

### 5.1. Normal code documentation is done using // and /** */

#### 5.1.1. Single lines

For example for a single line comment...

```php
// This line is used to output the contents of $a to the socket, this is how much the client paid us
echo $paidAmount;
```

#### 5.1.2. Multi-line

For example for a multi-line comment...

```php
/**
 * Hello world, its a great day.
 * Something else.
 */
echo $paidAmount;
```

#### 5.1.3. Alignment of comments

All comments must be aligned to the current indentation level of code.

#### 5.1.4. All code logic, flow, purpose and intentions should be documented with single line comments

To assist with readability all code should be commented on as much as is needed to understand
the intention and purpose of it without having to interpret the actual code.

### 5.2. Documenting function usage

Here's an example of a documented php function...

```php
/**
 * @brief this function does something with stuff
 *
 * @param $stuff The stuff that makes the function do something
 *
 * @return What's left of the stuff after something had been done to it
 */
function doSomthing($stuff)
{

}
```

### 5.3. Documenting classes

Here's an example of a documented php class...

```php
/**
 * @class DoStuff
 *
 * @brief This class is meant to do stuff (@brief is optional, but recommended to quickly describe a class)
 *
 * The stuff this class does is better explained with
 * a more detailed explanation which requires context
 * pertinent to the subject matter for that particular
 * knowledge domain.
 */
class DoStuff
{

	/**
	 * @var string $thingy
	 */
	private string $thingy;

	/**
	 * @method __construct
	 * This method constructs the object
	 */
	public function __construct()
	{
		$this->thingy = 'thingy default';
	}

	/**
	 * @method setThingy
	 * This method sets the value of thingy
	 */
	public function setThingy($thingy)
	{
		$this->thingy = $thingy
	}

	/**
	 * @method getThingy
	 * This method returns the value of thingy
	 * @return $thingy
	 */
	public function getThingy()
	{
		return $this->thingy;
	}

}
```

## 6. SQL

### 6.1. All SQL queries must have keywords capitalized

For example...
```sql
SELECT id FROM table;
```

## 6.2. Formatting SQL queries to fit 1 or multiple lines within 132 chars
### 6.2.1. SQL queries which involve more than 1 column, or 2 tables must be multi-line formatted

For example...
```php
$result = $dbh->query("
	INSERT INTO mytable
		(
			created,
			code,
			message
		)
	VALUES
		(
			?,
			?,
			?
		)
	",array(
		now(),
		100,
		"Hi there"
	)
);
```

Select queries...
```php
$result = $dbh->query("
		SELECT
			tblclients.firstname, tblclients.lastname, tblclients.companyname,
			tblproducts.name AS productname,
			tblcustomfieldsvalues.fieldid as customfieldid, tblcustomfieldsvalues.relid as hid,
			tblcustomfields.fieldname, tblcustomfieldsvalues.value as fieldvalue
		FROM
			tblcustomfields, tblcustomfieldsvalues, tblproducts, tblhosting, tblclients
		WHERE
			tblhosting.packageid = tblproducts.id
			AND tblcustomfields.type = 'product'
			AND tblcustomfields.fieldname IN (
				?,
				?,
				?
			)
			AND tblcustomfields.relid = tblproducts.id
			AND tblcustomfields.id = tblcustomfieldsvalues.fieldid
			AND tblcustomfieldsvalues.relid = tblhosting.id
			AND tblclients.id = tblhosting.userid
			AND tblclients.id != ?
		",array(
			$FIELD_VARIABLE_IDENTIFIER,
			$FIELD_VARIABLE_AMOUNT,
			$FIELD_VARIABLE_QUANTITY,
			38
		)
);
```

### 6.2.2. SQL queries which fit on one line can be abbreviated
Example... To account for the maximum line constraint of 132 chars it's best to place the entire query on it's own line
```php
$result = $dbh->query("SELECT thefield FROM thetable WHERE thingy = ?",array($thingie));
```

Rule of thumb is to print any query which spans more than one line on multiple lines.

### 6.2.3. SQL queries that aren't templated in any way to be indented by 1 tab
Any query that isn't templated, be it PDO prepared statements, object based queries or sprintf
are only to be indented 1 line from the same column as the first variable or identifier used in the statement
where the query is specified.

Example...
```php
$result = $dbh->query("
	SELECT
		thefield
	FROM
		thetable
	WHERE
		thingy = '1'
");
```

### 6.3. SQL quoting

Any and all variables included into SQL statements must be sufficiently quoted.

Its recommended to prepare the statement firts and execute it with the required parameters as this automatically quotes them.



CASE

##
Object properties are in lowercase

##
Object settings are in lowercase

##
Datastructure hashes are in camelcase with first character capatilized and abbreviations like SQL in caps.

##
Data structures being passed between applications or subsystems may require lowercasing attribute names

## 7. Control Structures

### 7.1. All control structures must be inline with their opening braces
```php
if ($condition) {
}

for (;;) {
}

while ($condition) {
}
```

### 7.2. When using else the previous if's closing brace must be on the same line and the else's opening brace
on the same line as well
```php
if ($condition) {

} else {

}
```

### 7.3. When using try/catch the opening brace must be on the same line as try and the catch's opening brace
on the same line as the try's closing brace
```php
try {

} catch (Exception $ex) {

}
```

## 7.4. switch, case
There MUST be a space after the switch keyword and the opening brace MUST be on the same line as the closing brace.
The case statement must be indented once from the switch and the break or terminating keyword must be indented once
from case.

Example...
```php
<?php
switch ($expr) {
    case 0:
        echo 'First case, with a break';
        break;
    case 1:
        echo 'Second case, which falls through';
        // no break
    case 2:
    case 3:
    case 4:
        echo 'Third case, return instead of break';
        return;
    default:
        echo 'Default case';
        break;
}
```

## 7.5. Closures
Closures MUST be declared with a space after the function keyword, and a space before and after the use keyword.

The opening brace MUST go on the same line, and the closing brace MUST go on the next line following the body.

There MUST NOT be a space after the opening parenthesis of the argument list or variable list,
and there MUST NOT be a space before the closing parenthesis of the argument list or variable list.

In the argument list and variable list, there MUST NOT be a space before each comma, and there MUST be one space after each comma.

Closure arguments with default values MUST go at the end of the argument list.

A closure declaration looks like the following. Note the placement of parentheses, commas, spaces, and braces:
```php
<?php
$closureWithArgs = function ($arg1, $arg2) {
    // body
};

$closureWithArgsAndVars = function ($arg1, $arg2) use ($var1, $var2) {
    // body
};
```

Argument lists and variable lists MAY be split across multiple lines, where each subsequent line is indented once.
When doing so, the first item in the list MUST be on the next line, and there MUST be only one argument or variable per line.

When the ending list (whether or arguments or variables) is split across multiple lines, the closing parenthesis and opening
brace MUST be placed together on their own line with one space between them.

The following are examples of closures with and without argument lists and variable lists split across multiple lines.

Example...
```php
<?php
$longArgs_noVars = function (
    $longArgument,
    $longerArgument,
    $muchLongerArgument
) {
   // body
};

$noArgs_longVars = function () use (
    $longVar1,
    $longerVar2,
    $muchLongerVar3
) {
   // body
};

$longArgs_longVars = function (
    $longArgument,
    $longerArgument,
    $muchLongerArgument
) use (
    $longVar1,
    $longerVar2,
    $muchLongerVar3
) {
   // body
};

$longArgs_shortVars = function (
    $longArgument,
    $longerArgument,
    $muchLongerArgument
) use ($var1) {
   // body
};

$shortArgs_longVars = function ($arg) use (
    $longVar1,
    $longerVar2,
    $muchLongerVar3
) {
   // body
};
```

Note that the formatting rules also apply when the closure is used directly in a function or method call as an argument.

Example...
```php
<?php
$foo->bar(
    $arg1,
    function ($arg2) use ($var1) {
        // body
    },
    $arg3
);
```

## 8. HTML PHP Mixed
### 8.1. No indentation for php tags unless it's on 1 line
Example... opening and closing php tags at start of line
```
					<div>
						some html indented a few times...
					</div>
<?php
					// Code indented to the html
					$code = 'something';
?>
```

More multiple line examples
```
				<td align="center" colspan="10" >
<?php
					$total = $this->Paginator->counter(
						array(
							'format' => '%pages%'
						)
					);
?>
```

If the tags are on 1 line then indent with html
```
<strong>TEST: <?php echo htmlentities($test) ?></strong>
```

## 9. Namespaces with regards to Autoloading
### 9.1.1. Autoloading
 - A fully-qualified namespace and class must have the following structure \<Vendor Name>\(<Namespace>\)*<Class Name>
 - Each namespace must have a top-level namespace ("Vendor Name").
 - Each namespace can have as many sub-namespaces as it wishes.
 - Each namespace separator is converted to a DIRECTORY_SEPARATOR when loading from the file system.
 - The fully-qualified namespace and class is suffixed with .php when loading from the file system.
 - Alphabetic characters in vendor names, namespaces, and class names may be of any combination of lower case and upper case.

Example...

 - \Doctrine\Common\IsolatedClassLoader => /path/to/project/lib/vendor/Doctrine/Common/IsolatedClassLoader.php
 - \Symfony\Core\Request => /path/to/project/lib/vendor/Symfony/Core/Request.php
 - \Zend\Acl => /path/to/project/lib/vendor/Zend/Acl.php
 - \Zend\Mail\Message => /path/to/project/lib/vendor/Zend/Mail/Message.php

### 9.1.2. Autoloading Underscores in Namespaces and Class Names
 - Each _ character in the CLASS NAME is converted to a DIRECTORY_SEPARATOR. The _ character has no special meaning in the
namespace.

Example...
 - \namespace\package\Class_Name => /path/to/project/lib/vendor/namespace/package/Class/Name.php
 - \namespace\package_name\Class_Name => /path/to/project/lib/vendor/namespace/package_name/Class/Name.php

### 9.1.3. Autoloading Example Implementation
Below is an example function to demonstrate how the above proposed standards are autoloaded
```php
<?php

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require $fileName;
}
```

## 9.2. Namespaces
### 9.2.1. Code written for PHP 5.3 and after MUST use formal namespaces.
Example...
```php
// PHP 5.3 and later:
namespace Vendor\Namespace;
```

## 9.3. Namespaces and Use Declarations
When present, there MUST be one blank line after the namespace declaration.
When present, all use declarations MUST go after the namespace declaration.
There MUST be only 1 use keywordsd per declaration.
There MUST be 3 blank lines after the use block.

Example...
```php
namespace Vendor\Namespace;

use FooClass;
use BarClass as Bar;
use Vendor\Package\ClassName;



// rest of PHP code
```

## 99. End Of File

To make life easier with tabs, files must contain

```php
# vim: ts=4
```

as the last line.
