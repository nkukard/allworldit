Contributing to a Repository
============================

The right way to contribute changes to a repository.

## Setting Up Your Git

Setup Git global settings with the below commands...
```sh
git config --global user.name "Your Name"
git config --global user.email "your@email"
git config --global user.signingkey "gpg_fingerprint"

git config --global format.signoff true
git config --global core.whitespace trailing-space,space-before-tab,indent-with-non-tab
git config --global color.ui true
```

## Forking

* Log into [LinuxAssist Devlabs GitLab](https://gitlab.devlabs.linuxassist.net)
* Find the project you're wanting to change, click on the repository
* Top right hand side, click "Fork"

You now have your own copy of the repository.

## Clone Locally

Clone the repository locally...

```sh
git clone ssh://git@gitlab.devlabs.linuxassist.net/yourusername/yourreponame.git
cd yourreponame
git config format.signature true
```

You should probably add the upstream repository too so you can do merges and rebases...

```sh
git remote add upstream ssh://git@gitlab.devlabs.linuxassist.net/upstream/repo.git
```

## Make Your Changes

Make and commit your changes, merge your commits, go mad.

### Branching

For each separate change you going to make you must create a branch...

```sh
git checkout upstream/master -b your-change-name
```

### Committing Your Changes

The first thing you need to do is review your changes using the following command...
```sh
git diff --staged
```

If you are 100% happy, commit your chagnes as usual, making sure of course all your changes must contain the Signed-Off line.

#### Adding Hunk-By-Hunk

You can add hunk by hunk, or group of hungs by group of hunks using the following...

```sh
git add --patch <filename>
```

Type ? at the prompt to get further help. The (e)dit option is VERY useful to pull in only changes that are surgical.

#### Removing Hunk-By-Hunk

If you mistakenly git add --patch a wrong hunk or need to ammend it use the following...

```sh
git reset --patch <filename>
```

#### Cherry-picking Your Changes

Sometimes you may find you have a commit in another branch you wish to submit, this is very easy to do. Git has a cherry-pick command which will fetch the commit and apply it to the branch you're in.

```sh
git cherry-pick de20a8566bf7a996d4bc8bde997fc359560f2b28
```

Remember to review the number of commits in your MR. Typically there should only be 1. See the below section about "Squashing Your Commits".



## Submitting Your Changes

Be sure to read the code [Submission Policy](https://gitlab.devlabs.linuxassist.net/allworldit/allworldit/blob/master/Submission%20Policy.md)

### Push Changes

Make sure your code is based off the main repo and push your branch...

```sh
git fetch upstream
git checkout yourbranchname
git rebase upstream/master
git push origin yourbranchname:descriptive-name
```

### Submit Upstream

Using the web interface you can then create a merge request:
* Click on your repository, click "Create Merge Request"
* Select the repository and branch on the top left and top right
* Type in a description of your changes
* Click "Submit merge request"

## Post-Review Changes

If you get comments on your code and you need to make changes to the branch merge request, its really easy to do.

### Fixing Your Code

Go through each comment one by one and commit the fixes as required.

### Squashing Your Commits

You do NOT want to submit 20,000 commits, squashing will take all the changes and squash them into 1 commit. Replace X with how many extra comments you made PLUS 1 to include your original commit.

To squash all commits since "master", use...

```sh
git rebase -i upstream/master
```

To squash X number of commits use the following...

```sh
git rebase -i HEAD~X
```

In all cases will be presented with a list of commits. Replace `pick` with `s` on all but your original commit. The next step will present you with a commit message with all your commit messages included for edit.

### Re-Submitting

You can now re-submit your changes by forcing a ref update...

```
git push -f origin yourbranchname:same-descriptive-name
```

## Further Changes

There are multiple ways to contribute further changes.

### Simply Branch Main Repo (easy way)

Branch the original repo and apply your changes to it.

```sh
git fetch upstream
git checkout upstream/master -b yourbranchname
git push origin yourbranchname:descriptive-name
```

### Bringing Your Branch Up To Date

Rebase your changes off the main repo. Make sure you are in your branch with your changes committed.

```sh
git fetch upstream
git rebase upstream/master
```

Push your changes...

```sh
git push origin yourbranchname:descriptive-name
```

